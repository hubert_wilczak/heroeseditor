<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="heroes-index">



    <div class="d-flex justify-content-around" id="buttons">
         <?= Html::a('Upload Heroes', ['create'], ['class' => 'btn btn-success', 'id' => 'create_button']) ?>
        <a href="index.php?r=heroes%2Fpdf" target="_blank">  <button type="button" id="pdf_btn"  class="btn btn-primary" > Print .pdf </button> </a>
    </div>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            [
                'attribute' => 'Name',
                'content' => function($model){
                    return '<b><i>'.$model->name." </i></b>";
                },

            ],
            'brilliance',
            'creativity',
            'enthusiasm',
            //'peace',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
