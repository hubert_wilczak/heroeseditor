<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Heroes */

$this->title =  $model->name;

?>
<div class="heroes-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
