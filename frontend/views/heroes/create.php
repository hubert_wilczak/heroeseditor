<?php


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Heroes */

$this->title = 'Upload file';

?>
<div class="heroes-create">

    <div class="d-flex justify-content-center mt-5">

        <?php \yii\widgets\ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data']
        ]) ?>

            <div class="icon_upload">


                <i class="fas fa-file-upload"></i>
            </div>

        <div class="text-muted mb-3">
            Only .xlsx files allowed
        </div>

        <button class="btn btn-primary btn-file">
            Choose file
            <input type="file" name="xlsxFile" id="uploadedFile">
        </button>



            <?php \yii\widgets\ActiveForm::end() ?>

    </div>

    <div class="error_text">
        <?php
        $session = Yii::$app->session;
        if($session->isActive)
        {
            echo $session->get('error');
            $session->destroy();
        }

        ?>
    </div>

</div>
