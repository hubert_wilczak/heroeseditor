<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Heroes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="heroes-form">

    <div class="update_content">

        <h1 style="text-align: center"><?= Html::encode($this->title) ?></h1>


        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->input('text', ['id' => 'name']) ?>

        <?= $form->field($model, 'brilliance')->input('number', ['id' => 'bri']) ?>

        <?= $form->field($model, 'creativity')->input('number', ['id' => 'cre']) ?>

        <?= $form->field($model, 'enthusiasm')->input('number', ['id' => 'ent']) ?>

        <?= $form->field($model, 'no_party')->input('number', ['id' => 'no_party']) ?>


        <div class="d-flex justify-content-around">

            <button id="calculation_button" type="button" class="btn btn-primary" onclick="setStan()">  MAKE CALCULATIONS  </button>




            <div class="form-group">
                <?= Html::submitButton('SAVE CHANGES', ['class' => 'btn btn-success', 'id' => 'calculation_button']) ?>
            </div>

        </div>

        <div id="table_count" class="d-flex justify-content-around">
            <table id="counting_tbl">

                <tr>
                    <td> Name </td>
                    <td> creativity </td>
                    <td> brilliance </td>
                    <td> enthusiasm </td>
                </tr>

                <tr>
                    <td> <div id="valueName"></div> </td>
                    <td> <div id="valueCre"></div> </td>
                    <td> <div id="valueBri"></div> </td>
                    <td> <div id="valueEnt"></div> </td>
                </tr>
            </table>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
