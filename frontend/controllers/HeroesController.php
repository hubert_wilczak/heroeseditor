<?php

namespace frontend\controllers;



use Mpdf\Mpdf;
use SimpleXLSX;
use Yii;
use frontend\models\Heroes;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * HeroesController implements the CRUD actions for Heroes model.
 */
class HeroesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Heroes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Heroes::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Heroes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Heroes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        require_once 'SimpleXLSX.php';
        $model = new Heroes();



        $file = UploadedFile::getInstanceByName('xlsxFile');

       if(!empty($file)) {
           if($file->extension != 'xlsx')
           {
                $session = Yii::$app->session;
                if($session->isActive){
                    $session->open();
                    $_SESSION['error'] = "Wrong file extension!";
                    return $this->redirect(['create']);
                }

           }
        }

        // $shift --> symbolizes the number of rows to skip
        $shift = 1;

        if(!empty($file))
        {
            if($xlsx = SimpleXLSX::parse($file->tempName))
            {
                $model->file_array = $xlsx->rows();

                foreach ($model->file_array as $row)
                {
                    if (!empty($row[1]))
                    {
                        if ($shift == 0)
                        {
                            $model->name = $row[1];
                            $model->enthusiasm = $row[2];
                            $model->creativity = $row[3];
                            $model->brilliance = $row[4];
                            $model->peace = $row[5];
                            $model->save();

                        }
                        else $shift = 0;
                    }
                }
            }
        }

        if ( Yii::$app -> request -> isPost) {
            return $this->redirect(['index', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Heroes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id -1]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Heroes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Heroes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Heroes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Heroes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPdf()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Heroes::find(),
        ]);

        $tab =  $dataProvider->getModels();


        $string = '<style>
  
           tr{ border-bottom: 1px solid black; }
           table{ width:100%; border:1px solid #555; }
           tr,td,th{ margin:0; padding:5px; }
           .be{ font-weight: bold; border-bottom: 1px solid black; }
           footer { margin-top: 10px; }
           
            </style>
            <h1>Characters table</h1>
            
            <table id="table_id" class="display">';

        $string = $string."<tr> <td class='be'  > Name </td> <td class='be'  > brilliance </td> <td class='be'  > creativity </td><td class='be'  > enthusiasm </td> </tr>";
        $string = $string."<tr> <td> Hubert Wilczak </td> <td> 432 </td> <td> 621 </td><td> 541 </td> </tr>";

        foreach ($tab as $row){
            $string =  $string."<tr> <td> ".$row->name." </td> <td> ".$row->brilliance." </td> <td> ".$row->creativity." </td><td> ".$row->enthusiasm." </td> </tr>";
        }



        $string = $string.'</table> <footer>Author: Hubert Wilczak</footer> ';

        //echo  $string;

        $mpdf = new Mpdf();
        $mpdf->WriteHTML($string);
        $mpdf->Output();


        return $this->render('pdf');
    }
}
