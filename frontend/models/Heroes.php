<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%heroes}}".
 *
 * @property int $id
 * @property string $name
 * @property int $brilliance
 * @property int $creativity
 * @property int $enthusiasm
 * @property int $peace
 */
class Heroes extends \yii\db\ActiveRecord
{
    public $file_array;
    public $counter_saves;
    public $no_party;
    public $error;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%heroes}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'brilliance', 'creativity', 'enthusiasm', 'peace'], 'required'],
            [['brilliance', 'creativity', 'enthusiasm', 'peace'], 'integer'],
            [['name'], 'string', 'max' => 150],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'brilliance' => 'Brilliance',
            'creativity' => 'Creativity',
            'enthusiasm' => 'Enthusiasm',
            'peace' => 'Peace',
            'no_party' => 'Number of party with HXS',
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {

        $isInsert = $this->isNewRecord;
        $saved =  parent::save($runValidation, $attributeNames);

        if(!$saved)
        {

            return false;
        }


            $this->counter_saves++;
            $this->id++;


        if($this->counter_saves >= 1)
        {
            $this->isNewRecord = true;
        }


        return true;

    }
}
